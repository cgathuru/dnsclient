# DnsClient #

The program implements a dns lookup given a dns server and a domain name.
It allows customization of settings though command line parameters such
as port no, timeout and more.