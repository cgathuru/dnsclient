/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

/**
 *
 * @author Charles
 */
public class NameErrorException extends Exception {
    
    public NameErrorException(String message){
        super(message);
    }
    
    public NameErrorException(){
        super();
    }
}
