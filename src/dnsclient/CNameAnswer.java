/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;

/**
 *
 * @author Charles
 */
public class CNameAnswer extends DnsAnswer {
      
    String alias;
    /**
     * The RDATA field contains the name of an alias
     */
    
    @Override
    protected void readRData(DnsInputStream response) throws IOException {
        alias = response.readDomainName();
    }
    
    /**
     * Gets the name of the alias
     * @return The name of the alias
     */
    public String getAlias(){
        return this.alias;
    }
    
}
