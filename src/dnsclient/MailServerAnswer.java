/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;

/**
 *
 * @author Charles
 */
public class MailServerAnswer extends DnsAnswer {
    
    /**
     * The RDATA field will the preference given to the resource.
     * Lower values are preferred.
     */
    private int preference;
    
    /**
     * The RDATA field will contain the domain name of the 
     * server. It can be parsed like the QNAME field.
     */
    private String domainName;

    @Override
    protected void readRData(DnsInputStream response) throws IOException {
        preference = response.readShort();
        domainName = response.readDomainName();      
    }
    
    public int getPreference(){
        return this.preference;
    }
    
    public String getDomainName(){
        return this.domainName;
    }
    
}
