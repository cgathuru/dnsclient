/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;

/**
 * 
 * @author Charles
 */
public class HostAnswer extends DnsAnswer {

    private String ipAddress;
    
    /**
     * This class parses the RDATA field as groups of
     * 4 octets, representing IP addresses. Each address
     * is 32 bits
     */
    
    /**
     * T
     * @param response
     * @throws java.io.IOException
     */
    @Override
    protected void readRData(DnsInputStream response) throws IOException {
        long ipVal = response.readInt();
        int[] ipInt = new int[4];       
        ipInt[0] = (int)(ipVal & 0xFF);
        ipInt[1] = (int) ((ipVal >> 8) & 0xFF);
        ipInt[2] = (int) ((ipVal >> 8*2) & 0xFF);
        ipInt[3] = (int) ((ipVal >> 8*3) & 0xFF);
        ipAddress = ""+ ipInt[3] +  "." + ipInt[2] +  "." + ipInt[1] 
                +  "." + ipInt[0];
    }
    
    /**
     * Gets the ipAdress in HostAnswer
     * @return The IpAddress parsed in rData
     */
    public String getIpAddress(){
        return this.ipAddress;
    }
    
}
