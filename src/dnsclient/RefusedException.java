/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

/**
 *
 * @author Charles
 */
public class RefusedException extends Exception{
    
    public RefusedException(String message){
        super(message);
    }
    
     public RefusedException(){
        super();
    }
    
}
