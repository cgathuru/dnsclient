/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

/**
 *
 * @author Charles
 */
public class Dns {
    
    public static final int SHIFT_QUERY = 15;
    public static final int SHIFT_OPCODE = 11;
    public static final int SHIFT_AUTHORITATIVE = 10;
    public static final int SHIFT_TRUNCATED = 9;
    public static final int SHIFT_RECURSE_PLEASE = 8;
    public static final int SHIFT_RECURSE_AVAILABLE = 7;
    public static final int SHIFT_RESPONSE_CODE = 0;
    
    private static final int queryClass = 1;
    
    
    private int opCode;
    private int qrCode;
    private int id;
    private int qdCount;
    private int ansCount;
    private int nsCount;
    private int arCount;
    private int rCode;
    

    
    public Dns(){

    }
    
    public byte[] generateQuery(String host, int queryType) throws IOException {
        Random rand = new Random();
        id = rand.nextInt();
        qrCode = 0;
        opCode = 0;
        qdCount = 1;
        ansCount = 0;
        nsCount = 0;
        arCount = 0;
        /** 
         * Usage from here http://stackoverflow.com/questions/2984538/how-to-use-bytearrayoutputstream-and-dataoutputstream-simultaneously-java
         */
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream serverRequest = new DataOutputStream(byteArrayOutputStream);
        
        
        serverRequest.writeShort(id);
        serverRequest.writeShort(( qrCode << SHIFT_QUERY) | 
                (opCode << SHIFT_OPCODE) | (1 << SHIFT_RECURSE_PLEASE));
        serverRequest.writeShort(qdCount);
        serverRequest.writeShort(ansCount);
        serverRequest.writeShort(nsCount);
        serverRequest.writeShort(arCount);
        StringTokenizer labels = new StringTokenizer (host, ".");
        while(labels.hasMoreTokens()) {
            String label = labels.nextToken();
            serverRequest.writeByte(label.length());
            serverRequest.writeBytes(label);
        }
        serverRequest.writeByte(0);
        serverRequest.writeShort(queryType);
        serverRequest.writeShort(queryClass);
        return byteArrayOutputStream.toByteArray();      
    }
    
    private boolean authoritative, truncated, recursive;
    private ArrayList<DnsAnswer> answers = new ArrayList();
    private ArrayList<DnsAnswer> authorities = new ArrayList();
    private ArrayList<DnsAnswer> additional = new ArrayList();
    
    public void readResponse(byte[] data,  int length) throws IOException, 
            IllegalStateException, FormatErrorException, 
            ServerFailureException, NameErrorException, 
            NotImplementedException, RefusedException {
        DnsInputStream response = new DnsInputStream(data, 0, length);
        id = response.readShort();
        int flags = response.readShort();
        readFlags(flags);
        qdCount = response.readShort();
        ansCount = response.readShort();
        nsCount = response.readShort();
        arCount = response.readShort();
        
        int numAns = ansCount;
        int numAuth = nsCount;
        int numAdd = arCount;
        
        while(qdCount-- > 0 ){
            response.readDomainName();
            response.readShort();
            response.readShort();
        }
        
        while(numAns -- > 0){
            answers.add(response.readDnsAnswer());
        }
        
        while(numAuth-- > 0 ){
            authorities.add(response.readDnsAnswer());
        }
        
        while(numAdd-- > 0 ){
           additional.add(response.readDnsAnswer());
        }
        printResponse();
    }
    
    public void readFlags(int flags) throws IllegalStateException, 
            FormatErrorException, ServerFailureException, 
            NameErrorException, NotImplementedException, RefusedException {
        opCode = (flags >> SHIFT_OPCODE) & 15;
        authoritative = (((flags >> SHIFT_AUTHORITATIVE) & 1) == 0)? false : true;
        truncated = (((flags >> SHIFT_TRUNCATED) & 1) == 0)? false : true;
        recursive = (((flags >> SHIFT_RECURSE_AVAILABLE) & 1) == 0)? false : true;
        rCode = (flags) & 15;
        relayErrorCode();
    }
    
    private void relayErrorCode() throws FormatErrorException, ServerFailureException, NameErrorException, NotImplementedException, RefusedException{
        switch(rCode) {
            case 0:
                break;
            case 1:
                throw new FormatErrorException("The name server was "
                        + "unable to interpret the query");
            case 2:
                throw new ServerFailureException("The name server was "
                        + "unable to process this query due to a problem "
                        + "with the name server");
            case 3:
                throw new NameErrorException("Domain name referenced in"
                        + " the query does not exist");
            case 4:
                throw new NotImplementedException("The name server does not "
                        + "support the requested kind of query");
            case 5:
                throw new RefusedException("The name server refueses to perform "
                        + "the requested opertaion for policy reasons");
            default:
                throw new IllegalStateException("Rcode value cannot be interpreted");
        }
        
    }
    
    public boolean isAuthoritative(){
        return this.authoritative;
    }
    
    public String getAuthoritative(){
        return isAuthoritative()? "auth" : "nonauth";
    }
    
    public boolean isTruncated(){
        return this.truncated;
    }
    
    public boolean isRecursive(){
        return this.recursive;
    }
    
    public int getOpCode(){
        return this.opCode;
    } 
    
    public void printResponse(){
        System.out.println("***Answer Section (" +
                answers.size() + " records)***");
        if(ansCount > 0) {
            printAnswers();
        } else {
            System.out.println("NOTFOUND");
        }
        System.out.println("***Additional Section (" +
                additional.size() + " records)***");
        if(arCount > 0) {
            printAdditional();
        } else {
            System.out.println("NOTFOUND");
        }
        
        
    }
    
    public void printAnswers(){      
        printRecords(answers);
       
    }
    
    public void printAdditional(){
        printRecords(additional);
    }
    
    public void printRecords(ArrayList<DnsAnswer> records){
        for(int i=0; i< records.size(); i++){
            if(records.get(i) instanceof HostAnswer){
                System.out.println("IP\t" + ((HostAnswer)records.get(i)).getIpAddress()
                +"\t" +records.get(i).getRTtl() +" \t" + getAuthoritative() );
            } else if(records.get(i) instanceof CNameAnswer){
                System.out.println("CNAME \t " +  ((CNameAnswer)records.get(i)).getAlias()
                + " \t " + ((CNameAnswer)records.get(i)).getRTtl() + " \t " + getAuthoritative());
            } else if(records.get(i) instanceof NameServerAnswer){
                System.out.println("NS \t " +  ((NameServerAnswer)records.get(i)).getNameServer()
                + " \t " + ((NameServerAnswer)records.get(i)).getRTtl() + " \t " + getAuthoritative());
            } else if(records.get(i) instanceof MailServerAnswer){
                System.out.println("MX \t " +  ((MailServerAnswer)records.get(i)).getDomainName()
                + " \t " + ((MailServerAnswer)records.get(i)).getRTtl() + " \t " + getAuthoritative());
            }
       }
    }
}
