/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * This contains the main class that is response for sending and 
 * receiving a DNS response as well as printing the response. It
 * is also responsible for the handling of exceptions that are thrown
 * during execution.
 * @author Charles
 */
public class DnsClient {
    
    public static final String MAIL_SERVER = "mx";
    public static final String NAME_SERVER = "ns";
    public static final String IP_ADDRESS = "a";
    public static final String C_NAME = "cname";

    /**
     * The main method.
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        // TODO code application logic here
        int timeout = 5;
        int maxRetries = 3;
        int port = 53;
        int requestType = 1;
        String server = "";
        String name = "";
        if (args.length > 9) {
            System.out.println("Error: There are too many arguments. Pleae re-check "
                    + "and try again");
        }
        for (int i = 0; i < args.length; i++ ) {
            if (args[i].equalsIgnoreCase("-t")) {
                try {
                    timeout = Integer.parseInt(args[++i]);
                } catch (NumberFormatException ex) {
                    System.out.println("Error: " + args[i] + " is not a valid timeout."
                            + " Using default " + timeout);
                }
            }
            else if ( args[i].equalsIgnoreCase("-r")) {
                try {
                    maxRetries = Integer.parseInt(args[++i]);
                } catch (NumberFormatException ex) {
                    System.out.println("Error: " + args[i] + " is not a valid timeout."
                            + " Using default " + maxRetries);
                }
            }
            else if ( args[i].equalsIgnoreCase("-p")) {
                try {
                    port = Integer.parseInt(args[++i]);
                } catch (NumberFormatException ex) {
                    System.out.println("Error: " + args[i] + " is not a valid port."
                            + " Using default " + port);
                }
            }
            else if (args[i].equalsIgnoreCase("-"+MAIL_SERVER)) {
                requestType = 15;
            }
            else if (args[i].equalsIgnoreCase("-"+NAME_SERVER)) {
                requestType = 2;
            }
            else if (args[i].contains("@")) {
                server = args[i].substring(1);
            }
            else {
                name = args[i];
            }
        }

        checkDefaultArgs(server, name);
            DatagramSocket clientSocket = null;
        try {    
             clientSocket = new DatagramSocket(port);
            byte[] sendData = new Dns().generateQuery(name, requestType);
            DatagramPacket sendPacket = new DatagramPacket(
                    sendData, sendData.length, getIpAddress(server), port);
            
            int tries = -1;
            byte[] receiveData = new byte[1024];
            long timeElapsed = 0;
            DatagramPacket receivePacket = new DatagramPacket(
                            receiveData, receiveData.length);;
            while(tries < maxRetries+1){
                try {                  
                    clientSocket.send(sendPacket);
                    clientSocket.setSoTimeout( timeout*1000 ) ;
                    long intitalTime = System.currentTimeMillis();
                    
                    clientSocket.receive(receivePacket);
                    timeElapsed = System.currentTimeMillis() - intitalTime;
                    break;
                } catch (SocketTimeoutException ex) {
                    tries++;
                    if(tries == maxRetries){
                        throw new SocketTimeoutException(ex.getMessage() + 
                                ". Max number of retries exceeded: " + maxRetries);
                    }
                }
            } 
            
            System.out.println("DnsClient sending request for " + name);
            System.out.println("Server: " + server);
            System.out.println("Request type: " + 
                    getResponseType(requestType).toUpperCase());
            System.out.println("Response received after " + (double)timeElapsed/1000 +
                    " seconds (" + (tries+1) + " retries)");
            
            Dns response = new Dns();
            response.readResponse(receivePacket.getData(), receivePacket.getLength());                 
            
        } catch (SocketException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (UnknownHostException ex){
            System.out.println("Error: " + "Unable to convert ipAdress into useable format");
        } catch (NumberFormatException ex){
            System.out.println("Error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (IllegalStateException | FormatErrorException | 
                ServerFailureException |  NameErrorException | 
                NotImplementedException | RefusedException | 
                IllegalArgumentException ex) {
            System.out.println("Error: " + ex.getMessage());
        } 
        finally {
            if (clientSocket != null)
                clientSocket.close();
        }

    }
                      
    
    public static void checkDefaultArgs(String server, String name) {
        if("".equals(server)) {
            System.out.println("Error: Required server is missing. Please re-run "
                    + "with a server");
            System.exit(1);
        }
        if("".equals(name)) {
           System.out.println("Error: Required domain name is missing. Please re-run"
                    + "with a domain name");
            System.exit(1); 
        }
    }
    
    public static InetAddress getIpAddress(String address) 
            throws UnknownHostException, IllegalArgumentException,
            NumberFormatException{

        byte[] ipBytes = new byte[4];
        int index = 0;
        StringTokenizer token = new StringTokenizer (address, ".");
        
        if(token.countTokens() != 4 ){
            throw new IllegalArgumentException("The IP address provided "+ address +" "
                    + "could not be interpreted");
        }
    
        while ( token.hasMoreTokens()) {
            //  Get the current token and convert to an integer value
            String ipNum = token.nextToken();
            int ipVal;
            try{
            ipVal = Integer.valueOf(ipNum);
            } catch(NumberFormatException e){
                throw new NumberFormatException("Unable to convert " + ipNum + " to an integer");
            }
            //  Validate the current address part
            if ( ipVal < 0 || ipVal > 255)
              return null;
            ipBytes[index++] = (byte) (ipVal & 0xFF);
        }
    
        return InetAddress.getByAddress(ipBytes);
    }
    
    public static String getResponseType(int rType){
        switch(rType){
            case  1:
                return IP_ADDRESS;
            case 2:
                return NAME_SERVER;
            case 5:
                return C_NAME;
            case 15:
                return MAIL_SERVER;
            default:
                return "";
        }
    }
}
