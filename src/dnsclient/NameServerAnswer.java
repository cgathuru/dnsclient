/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;

/**
 *
 * @author Charles
 */
public class NameServerAnswer extends DnsAnswer {

    private String nameServer;
    /**
     * The RDATA field will contain the name of the 
     * server. It can be parsed like the QNAME field.    /**
     * The RDATA field will contain the name of the 
     * server. It can be parsed like the QNAME field.
     */
    
    /**
     * Reads a name server from the input stream
     * @param response The input stream to read from
     * @throws java.io.EOFException Thrown if unable to read name server
     */
    @Override
    protected void readRData(DnsInputStream response) throws IOException{
        nameServer = response.readDomainName();
    }
    
    /**
     * 
     * @return The name server
     */
    public String getNameServer(){
        return this.nameServer;
    }
     
}
