/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

/**
 *
 * @author Charles
 */
public class ServerFailureException extends Exception{
    
    public ServerFailureException(String message){
        super(message);
    }
    
    public ServerFailureException(){
        super();
    }
    
}
