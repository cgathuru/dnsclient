/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 *
 * @author Charles
 */
public class DnsInputStream extends ByteArrayInputStream {
    
    protected DataInputStream dataInput;

    public DnsInputStream(byte[] buf, int offset, int length) {
        super(buf, offset, length);
        dataInput = new DataInputStream(this);
    }
    
    /**
     * Reads an U16 from the stream
     * @return An integer that contains the U16
     * @throws IOException 
     */
    public int readShort() throws IOException {
        return dataInput.readUnsignedShort();
    }
    
    /**
     * Reads an U32 from the stream
     * @return A long that contains the U32
     * @throws IOException 
     */
    public long readInt() throws IOException {
        return dataInput.readInt() & 0xffffffffL;
    }
    
    /**
     * Reads an unsigned byte from the stream
     * @return An integer that contains the byte
     * @throws IOException 
     */
    public int readByte() throws IOException {
        return dataInput.readUnsignedByte();
    }
    
    public String readString() throws IOException {
        int length = readByte();
        if (length != 0) {
            byte[] buffer = new byte[length];
            dataInput.readFully(buffer);
            return new String(buffer);
        } else {
            return "";
        }
    }
    
    public String readDomainName() throws EOFException, IOException {
        if(pos >= count){
            // We have reached end of file
            throw new EOFException("EOF reached reading the domain name");
        }
        // check if its a domain name or pointer
        // first two bits are 11 if its a pointer
        if ((buf[pos] & 0xc0) == 0xc0) {
                int offset = readShort() & 0x3fff;
                return new DnsInputStream(buf, offset, buf.length - offset)
                        .readDomainName();
        } else if( (this.buf[pos] & 0xc0) == 0) {
            // there is no compression
            String label = readString();
            if(label.length() > 0) { // we havent reached the end
                String remainder = readDomainName();
                if( remainder.length() > 0)
                    label = label + '.' + remainder;
            }
            return label;
        } else {
            /* If we got here we do not know what the compression is
            *  because its not a valid compression
            */
            throw new IOException("Compression offest not valid");
        }
        
    }   
    
    public DnsAnswer readDnsAnswer() throws IOException, IllegalStateException{
        String domainName = readDomainName();
        int rType = readShort();
        int rClass = readShort();
        if(rClass != 1){
            throw new IllegalStateException("The qType value encountered was"
                    + " unexpected while decoding dns answer for " + domainName + ". "
            + "Expected 1 and got" + rClass);
        }
        long ttl = readInt();
        int rdLength = readShort();
        DnsAnswer dnsAnswer;
        switch(rType){
            case 1:
                dnsAnswer = new HostAnswer();
                break;
            case 2:
                dnsAnswer = new NameServerAnswer();
                break;
            case 5:
                dnsAnswer = new CNameAnswer();
                break;
            case 15:
                dnsAnswer = new MailServerAnswer();
                break;
            default:
                // We should not have got here
                throw new IllegalStateException("Unknown type " + rType + ". Unable to decode response further.");
        }
        DnsInputStream rDnsStream = new DnsInputStream(buf,pos,rdLength);
        pos += rdLength;
        dnsAnswer.initialize(domainName, rType, rClass, ttl, rDnsStream);
        
        return dnsAnswer;
    }

    
}
