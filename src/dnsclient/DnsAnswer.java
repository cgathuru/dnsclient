/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.io.IOException;

/**
 * This class contains a DNS Answer. All required fields pertaining to
 * a DNS Answer structure are contained here.
 * @author Charles
 */
public abstract class DnsAnswer {
    
    private String rName;
    private int rType;
    private int rClass;
    private long rTtl;
    
    /*
    * Decided to use an initalize method here because I cant call an abstract
    * method readRData() from a constructor
    */
    
    /**
     * This method must be called in place of the constructor to initialize all
     * the variables
     * @param rName The domain name to which this record pertains
     * @param rType The type of answer
     * @param rClass The Class
     * @param rTtl The TTL
     * @param dnsStream The remaining bytes of the answer to be read
     */
    void initialize(String rName, int rType, int rClass, 
            long rTtl, DnsInputStream dnsStream) throws IOException {
        this.rName = rName;
        this.rType = rType;
        this.rClass = rClass;
        this.rTtl = rTtl;
        readRData(dnsStream);
    }
    
    /**
     * Reads the remaining data that is contained in the rData field.
     * @param response The @DnsInputStream containing the rData field
     * @throws IOException If there is error when reading the stream
     */
    protected abstract void readRData(DnsInputStream response) throws IOException;
    
    
    /**
     * Gets the RName
     * @return The rName
     */
    public String getRName(){
      return this.rName;  
    }
    
    /**
     * Gets the TTL
     * @return The TTL
     */
    public long getRTtl(){
        return this.rTtl;
    }
    
}
