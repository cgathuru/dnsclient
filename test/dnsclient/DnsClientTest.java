/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnsclient;

import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charles
 */
public class DnsClientTest {
    
    public DnsClientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class DnsClient.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = {"@132.204.8.207", "www.google.ca"};
        DnsClient.main(args);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of main method, of class DnsClient.
     */
    @Test
    public void testMainGoogle() {
        System.out.println("main");
        String[] args = {"@8.8.8.8", "www.google.ca"};
        DnsClient.main(args);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkDefaultArgs method, of class DnsClient.
     */
    @Test
    public void testCheckDefaultArgs() {
        System.out.println("checkDefaultArgs");
        String server = "blah";
        String name = "123445";
        DnsClient.checkDefaultArgs(server, name);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testMainGoogleWrongDns() {
        System.out.println("mainWrongDns");
        String[] args = {"@hi", "www.google.ca"};
        DnsClient.main(args);
    }
    
     @Test
    public void testMainGoogleWrongDomain() {
        System.out.println("mainWrongDomain");
        String[] args = {"@8.8.8.8", "happyfeet"};
        DnsClient.main(args);
    }
    
    @Test
    public void testMainGoogleInavlidPort() {
        System.out.println("mainGoogleInavlidPort");
        String[] args = {"@8.8.8.8", "-p", "fries", "www.google.ca", };
        DnsClient.main(args);
    }
    
    @Test
    public void testMainGooglePort58() {
        System.out.println("mainGooglePort58");
        String[] args = {"@8.8.8.8", "-p", "58", "www.google.ca", };
        DnsClient.main(args);
    }
    
    @Test
    public void testMainGooglePortNeg() {
        System.out.println("mainGoogleNegPort");
        String[] args = {"@8.8.8.8", "-p", "-53", "www.google.ca", };
        DnsClient.main(args);
    }

    /**
     * Test of getIpAddress method, of class DnsClient.
     */
    @Test
    public void testGetIpAddress() throws Exception {
        System.out.println("getIpAddress");
        String address = "192.168.1.1";
        InetAddress expResult = InetAddress.getByName(address);
        InetAddress result = DnsClient.getIpAddress(address);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
